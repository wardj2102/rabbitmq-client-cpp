# rabbitmq-client-cpp

Welcome to rabbitmq-client-cpp. 

This repository is an initial implentation of a rabbitmq client library in C++. This project is meant to accomplish a pika-like C++ library to interface with a rabbitmq server. While this library is still a WIP, the first version of the library will have a fully functioning BlockingConnection class implemented.

As of 1/20/20, AMQP protocol framework is working and functions like basic publish are ready to be written. 

## Docker build and launch commands
```docker volume create --name rabbitmq-client-cpp --opt type=none --opt device=/home/jward/rabbitmq-client-cpp --opt o=bind```

```docker run -it -v rabbitmq-client-cpp:/rabbitmq-client-cpp registry.gitlab.com/wardj2102/rabbitmq-client-cpp```

```docker build . -t registry.gitlab.com/wardj2102/rabbitmq-client-cpp:latest```

```docker run -d --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management```