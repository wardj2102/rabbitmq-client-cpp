// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "rabbitmq-client/connection.h"

namespace amqp::connection {

  Connection::Connection(std::shared_ptr<BaseConnection> connection_type)
      : connection_type(std::move(connection_type)) {}

  auto Connection::startConnectionHandshake()
      -> std::shared_ptr<connection::Start> {
    // create Protocal header

    connection_type->write(ProtocolHeader::getHeader());

    return FrameFactory::readFrame<connection::Start>(connection_type);
  }

  auto Connection::NegotiateAuthentication(const std::string &username,
                                           const std::string &password,
                                           const PeerProperties &properties)
      -> std::shared_ptr<connection::Tune> {

    std::shared_ptr<connection::StartOk> responseStartOkMethod =
        std::make_shared<connection::StartOk>();

    responseStartOkMethod->getClientProperties() = properties;
    responseStartOkMethod->getMechanism() = ShortString("PLAIN");
    responseStartOkMethod->getLocale() = ShortString("en_US");

    using namespace std::string_literals;
    responseStartOkMethod->getResponse() =
        LongString("\0"s + username + "\0"s + password);

    FrameFactory::writeMethod(responseStartOkMethod, connection_type,
                              channel_id);

    return FrameFactory::readFrame<connection::Tune>(connection_type);
  }

  void Connection::NegotiateTune(
      const std::shared_ptr<amqp::connection::Tune> &tune_method) {

    std::shared_ptr<amqp::connection::TuneOk> tune_ok =
        std::make_shared<amqp::connection::TuneOk>();
    tune_ok->getChannelMax() = tune_method->getChannelMax();
    tune_ok->getHeartbeat() = tune_method->getHeartbeat();
    tune_ok->getFrameMax() = tune_method->getFrameMax();

    FrameFactory::writeMethod(tune_ok, connection_type, channel_id);
  }

  void Connection::OpenConnection(const std::string &vhost) {
    std::shared_ptr<amqp::connection::Open> open_method =
        std::make_shared<amqp::connection::Open>();

    open_method->getPath() = ShortString(vhost);

    FrameFactory::writeMethod(open_method, connection_type, channel_id);

    std::shared_ptr<amqp::connection::OpenOk> open_ok_method =
        FrameFactory::readFrame<connection::OpenOk>(connection_type);

    spdlog::info("Connection Established");
  }

} // namespace amqp::connection
