// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include <rabbitmq-client/boost-connection.h>

namespace rabbitmqClient {

  auto BoostConnection::connect(const std::string &hostname,
                                const std::string &port) -> bool {

    resolver = std::make_shared<boost::asio::ip::tcp::resolver>(io_context);
    auto endpoints = resolver->resolve(hostname, port, ec);
    if (ec) { /*ups, error here */
      spdlog::warn(ec.message());

      return false;
    }

    socket = std::make_shared<boost::asio::ip::tcp::socket>(io_context);
    boost::asio::connect(*socket, endpoints, ec);

    if (ec) { /*ups, error here */
      spdlog::warn(ec.message());
      return false;
    }

    return true;
  }

  auto BoostConnection::read(std::vector<amqp::Octet> &payload) -> bool {
    ec.clear();

    socket->read_some(boost::asio::buffer(payload), ec);

    return true;
  }

  auto BoostConnection::write(const std::vector<amqp::Octet> &payload) -> bool {
    socket->write_some(boost::asio::buffer(payload), ec);

    return true;
  }

} // namespace rabbitmqClient