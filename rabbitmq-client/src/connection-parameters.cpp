// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "rabbitmq-client/connection-parameters.h"

namespace rabbitmqClient {
  PlainCredentials::PlainCredentials(std::string &&username,
                                     std::string &&password)
      : username(std::move(username)), password(std::move(password)) {}

  auto PlainCredentials::getUsername() const -> const std::string & {
    return username;
  }
  auto PlainCredentials::getPassword() const -> const std::string & {
    return password;
  }

  ConnectionParameters::ConnectionParameters(std::string &&hostname,
                                             std::string &&port,
                                             std::string &&vhost,
                                             PlainCredentials &credentials)
      : hostname(std::move(hostname)), port(std::move(port)),
        vhost(std::move(vhost)), credentials(std::move(credentials)) {}

  auto ConnectionParameters::getHostname() const -> const std::string & {
    return hostname;
  }
  auto ConnectionParameters::getPort() const -> const std::string & {
    return port;
  }
  auto ConnectionParameters::getVhost() const -> const std::string & {
    return vhost;
  }
  auto ConnectionParameters::getCredentials() const
      -> const PlainCredentials & {
    return credentials;
  }
} // namespace rabbitmqClient