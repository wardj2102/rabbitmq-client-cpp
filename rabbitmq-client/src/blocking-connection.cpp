// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include <rabbitmq-client/blocking-connection.h>

namespace rabbitmqClient {
  BlockingConnection::BlockingConnection(ConnectionParameters params)
      : params(std::move(params)),
        connection_type(std::make_shared<BoostConnection>()){};

  auto BlockingConnection::connect() -> bool {
    spdlog::info("Creating connection");

    connection_type->connect(params.getHostname(), params.getPort());

    amqp::connection::Connection c(connection_type);

    spdlog::info("Starting Connection Handshake");
    std::shared_ptr<amqp::connection::Start> start_method =
        c.startConnectionHandshake();

    spdlog::info("Negotiating Authentication");
    std::shared_ptr<amqp::connection::Tune> tune_method =
        c.NegotiateAuthentication(params.getCredentials().getUsername(),
                                  params.getCredentials().getPassword(),
                                  start_method->getServerProperties());

    spdlog::info("Negotiating Tune");
    c.NegotiateTune(tune_method);

    spdlog::info("Confirming Open Connection");
    c.OpenConnection(params.getVhost());

    // TODO: Fix me to support channel limits
    for (int i = 1; i < channel_limit; i++) {
      channel_selector.push(i);
    }

    is_connected = true;
    return is_connected;
  }

  auto BlockingConnection::channel() -> std::shared_ptr<Channel> {

    if (!is_connected) {
      spdlog::warn("Blocking connection is not connected, cannot make channel");
      return nullptr;
    }
    if (channel_selector.empty()) {
      spdlog::warn("Connection cannot support any more channels");
      return nullptr;
    }

    std::shared_ptr<Channel> channel =
        std::make_shared<Channel>(connection_type, channel_selector.front());

    if (!channel->open()) {
      spdlog::warn("Channel could not be opened");

      return nullptr;
    }
    spdlog::info("Channel opened sucessfully");
    active_channels.insert({channel_selector.front(), channel});
    channel_selector.pop();

    return channel;
  }

  auto BlockingConnection::isOpen() const -> const bool & {
    return is_connected;
  }

} // namespace rabbitmqClient