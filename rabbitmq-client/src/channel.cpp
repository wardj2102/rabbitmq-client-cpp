// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "rabbitmq-client/channel.h"

namespace rabbitmqClient {
  Channel::Channel(std::shared_ptr<amqp::BaseConnection> connection_type,
                   amqp::ShortUint channel_id)
      : connection_type(std::move(connection_type)), channel_id(channel_id) {}

  auto Channel::open() -> const bool & {
    std::shared_ptr<amqp::channel::Open> openMethod =
        std::make_shared<amqp::channel::Open>();

    amqp::FrameFactory::writeMethod(openMethod, connection_type, channel_id);

    amqp::FrameFactory::readFrame<amqp::channel::OpenOk>(connection_type);

    is_open = true;
    return isOpen();
  }

  auto Channel::isOpen() const -> const bool & { return is_open; }

  void Channel::basicPublish(
      const std::string &exchange,    // NOLINT(misc-unused-parameters)
      const std::string &routing_key, // NOLINT(misc-unused-parameters)
      const std::string &body,        // NOLINT(misc-unused-parameters)
      bool mandatory) const {         // NOLINT(misc-unused-parameters)
    if (!isOpen()) {
      spdlog::warn("Channel is not open");
      return;
    }
  }

} // namespace rabbitmqClient