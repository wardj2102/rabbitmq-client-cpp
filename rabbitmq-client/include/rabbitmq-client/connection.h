// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_CONNECTION_H
#define RABBITMQ_CLIENT_CONNECTION_H

#include <rabbitmq-client-amqp/amqp.h>
#include <spdlog/spdlog.h>

#include <iostream>
#include <queue>
#include <string>
#include <typeinfo>

namespace amqp::connection {

  class Connection {
  public:
    explicit Connection(std::shared_ptr<BaseConnection> connection_type);

    auto startConnectionHandshake() -> std::shared_ptr<connection::Start>;

    auto NegotiateAuthentication(const std::string &username,
                                 const std::string &password,
                                 const PeerProperties &properties)
        -> std::shared_ptr<connection::Tune>;

    void
    NegotiateTune(const std::shared_ptr<amqp::connection::Tune> &tune_method);

    void OpenConnection(const std::string &vhost);

  private:
    const std::shared_ptr<BaseConnection> connection_type;
    const ShortUint channel_id = 0;
  };
} // namespace amqp::connection

#endif // RABBITMQ_CLIENT_CONNECTION_H
