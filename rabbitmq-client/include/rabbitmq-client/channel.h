// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_CHANNEL_H
#define RABBITMQ_CLIENT_CHANNEL_H

#include <rabbitmq-client-amqp/amqp.h>
#include <spdlog/spdlog.h>

#include <iostream>
#include <string>
#include <typeinfo>

namespace rabbitmqClient {

  class Channel {
  public:
    explicit Channel(std::shared_ptr<amqp::BaseConnection> connection_type,
                     amqp::ShortUint channel_id);

    auto open() -> const bool &;
    [[nodiscard]] auto isOpen() const -> const bool &;

    void basicPublish(const std::string &exchange,
                      const std::string &routing_key, const std::string &body,
                      bool mandatory = false) const;

  private:
    const std::shared_ptr<amqp::BaseConnection> connection_type;
    const amqp::ShortUint channel_id;
    bool is_open = false;
  };
} // namespace rabbitmqClient

#endif // RABBITMQ_CLIENT_CHANNEL_H
