// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_CONNECTION_PARAMETERS_H
#define RABBITMQ_CLIENT_CONNECTION_PARAMETERS_H

#include <string>

namespace rabbitmqClient {

  class CredentialBase {};

  class PlainCredentials : public CredentialBase {
  public:
    PlainCredentials(std::string &&username, std::string &&password);
    [[nodiscard]] auto getUsername() const -> const std::string &;
    [[nodiscard]] auto getPassword() const -> const std::string &;

  private:
    const std::string username;
    const std::string password;
  };

  class ConnectionParameters {
  public:
    ConnectionParameters(std::string &&hostname, std::string &&port,
                         std::string &&vhost, PlainCredentials &credentials);

    [[nodiscard]] auto getHostname() const -> const std::string &;
    [[nodiscard]] auto getPort() const -> const std::string &;
    [[nodiscard]] auto getVhost() const -> const std::string &;
    [[nodiscard]] auto getCredentials() const -> const PlainCredentials &;

  private:
    const std::string hostname;
    const std::string port;
    const std::string vhost;
    const PlainCredentials credentials;
  };
} // namespace rabbitmqClient

#endif // RABBITMQ_CLIENT_CONNECTION_PARAMETERS_H