// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_BLOCKING_CONNECTION_H
#define RABBITMQ_CLIENT_BLOCKING_CONNECTION_H

#include <rabbitmq-client/boost-connection.h>
#include <rabbitmq-client/channel.h>
#include <rabbitmq-client/connection-parameters.h>
#include <rabbitmq-client/connection.h>

namespace rabbitmqClient {
  class BlockingConnection {
  public:
    explicit BlockingConnection(ConnectionParameters params);

    auto connect() -> bool;

    auto channel() -> std::shared_ptr<Channel>;

    auto isOpen() const -> const bool &;

  private:
    const ConnectionParameters params;
    const std::shared_ptr<amqp::BaseConnection> connection_type;
    bool is_connected = false;
    std::queue<amqp::ShortUint> channel_selector;
    std::unordered_map<amqp::ShortUint, std::shared_ptr<Channel>>
        active_channels;
    const amqp::ShortUint channel_limit = 11;
  };
} // namespace rabbitmqClient

#endif // RABBITMQ_CLIENT_BLOCKING_CONNECTION_H