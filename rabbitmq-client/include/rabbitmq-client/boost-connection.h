// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_BOOST_CONNECTION_H
#define RABBITMQ_CLIENT_BOOST_CONNECTION_H

#include <boost/asio.hpp>
#include <rabbitmq-client-amqp/amqp.h>
#include <spdlog/spdlog.h>

namespace rabbitmqClient {
  class BoostConnection : public amqp::BaseConnection {
  public:
    BoostConnection() = default;
    auto connect(const std::string &hostname, const std::string &port)
        -> bool final;
    auto read(std::vector<amqp::Octet> &payload) -> bool final;
    auto write(const std::vector<amqp::Octet> &payload) -> bool final;

  private:
    boost::asio::io_context io_context;
    std::shared_ptr<boost::asio::ip::tcp::resolver> resolver;
    boost::system::error_code ec;
    std::shared_ptr<boost::asio::ip::tcp::socket> socket;
  };
} // namespace rabbitmqClient
#endif // RABBITMQ_CLIENT_BOOST_CONNECTION_H
