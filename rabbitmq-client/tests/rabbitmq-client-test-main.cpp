// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

// #include <rabbitmq-client-test/rabbitmq-client-test.h>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

auto main(int argc, char **argv) -> int {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
