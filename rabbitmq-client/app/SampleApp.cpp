// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include <rabbitmq-client/blocking-connection.h>
#include <rabbitmq-client/connection.h>

auto main() -> int {
  rabbitmqClient::PlainCredentials authentication("guest", "guest");
  rabbitmqClient::ConnectionParameters params("localhost", "5672", "/",
                                              authentication);

  try {
    rabbitmqClient::BlockingConnection connection(params);
    if (!connection.connect()) {
      spdlog::error("Sample app connection did not open correctly");
      return 1;
    };

    std::shared_ptr<rabbitmqClient::Channel> channel = connection.channel();
    if (!channel->isOpen()) {
      spdlog::error("Sample app channel did not open correctly");
      return 1;
    };

    channel->basicPublish("test_exchange", "test_routing_key",
                          "message body value");
  } catch (...) {
  }
  return 1;
}
