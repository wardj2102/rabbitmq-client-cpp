// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <rabbitmq-client-amqp/amqp.h>

using namespace amqp;

// NOLINTNEXTLINE
TEST(ConvertTests, TestConvertBool) {
  std::vector<Octet> buffer{1};
  bool testBool = false;
  auto iter = buffer.begin();
  ConvertAmqpTypes::convertBufferToType(iter, testBool);

  ASSERT_EQ(testBool, true);

  testBool = false;
  iter = buffer.begin();
  ConvertAmqpTypes::convertTypeToBuffer(iter, testBool);

  ASSERT_EQ(buffer[0], 0);
}