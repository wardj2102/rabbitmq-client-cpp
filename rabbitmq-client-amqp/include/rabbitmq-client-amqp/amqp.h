// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_AMQP_AMQP_H
#define RABBITMQ_CLIENT_AMQP_AMQP_H

#include "base-connection.h"
#include "base-frames.h"
#include "basic-methods.h"
#include "channel-methods.h"
#include "connection-methods.h"
#include "exchange-methods.h"
#include "queue-methods.h"
#include "tx-methods.h"

namespace amqp {

  class Frame : public FrameBase {
  public:
    explicit Frame(const std::shared_ptr<BaseConnection> &connection_type);

    void read() final;

    void write() final;

    auto getGeneralFrame() -> GeneralFrame &;
    auto getType() -> std::shared_ptr<FrameBase::FrameTypeEnum> &;
    auto getMethod() -> std::shared_ptr<Method> &;

  private:
    GeneralFrame generalFrame;
    std::shared_ptr<FrameBase::FrameTypeEnum> type;
    std::shared_ptr<Method> method;

    template <typename T> auto makeMethod() -> std::shared_ptr<Method> {
      return std::make_shared<T>();
    }

    void lookUpMethod(Octet &type, std::vector<Octet>::iterator &iter,
                      std::shared_ptr<Method> &method);

    const std::unordered_map<int,
                             std::unordered_map<int, std::shared_ptr<Method>>>
        methodLookUpTable{
            {connection::CONNECTION_CLASS_ID,
             {{connection::START_METHOD_ID, makeMethod<connection::Start>()},
              {connection::STARTOK_METHOD_ID,
               makeMethod<connection::StartOk>()},
              {connection::SECURE_METHOD_ID, makeMethod<connection::Secure>()},
              {connection::SECUREOK_METHOD_ID,
               makeMethod<connection::SecureOk>()},
              {connection::TUNE_METHOD_ID, makeMethod<connection::Tune>()},
              {connection::TUNEOK_METHOD_ID, makeMethod<connection::TuneOk>()},
              {connection::OPEN_METHOD_ID, makeMethod<connection::Open>()},
              {connection::OPENOK_METHOD_ID, makeMethod<connection::OpenOk>()},
              {connection::CLOSE_METHOD_ID, makeMethod<connection::Close>()},
              {connection::CLOSEOK_METHOD_ID,
               makeMethod<connection::CloseOk>()}}},
            {channel::CHANNEL_CLASS_ID,
             {{channel::OPEN_METHOD_ID, makeMethod<channel::Open>()},
              {channel::OPENOK_METHOD_ID, makeMethod<channel::OpenOk>()},
              {channel::FLOW_METHOD_ID, makeMethod<channel::Flow>()},
              {channel::FLOWOK_METHOD_ID, makeMethod<channel::FlowOk>()},
              {channel::CLOSE_METHOD_ID, makeMethod<channel::Close>()},
              {channel::CLOSEOK_METHOD_ID, makeMethod<channel::CloseOk>()}}},
            {exchange::EXCHANGE_CLASS_ID,
             {{exchange::DECLARE_METHOD_ID, makeMethod<exchange::Declare>()},
              {exchange::DECLAREOK_METHOD_ID,
               makeMethod<exchange::DeclareOk>()},
              {exchange::DELETE_METHOD_ID, makeMethod<exchange::Delete>()},
              {exchange::DELETEOK_METHOD_ID,
               makeMethod<exchange::DeleteOk>()}}},
            {queue::QUEUE_CLASS_ID,
             {{queue::DECLARE_METHOD_ID, makeMethod<queue::Declare>()},
              {queue::DECLAREOK_METHOD_ID, makeMethod<queue::DeclareOk>()},
              {queue::BIND_METHOD_ID, makeMethod<queue::Bind>()},
              {queue::BINDOK_METHOD_ID, makeMethod<queue::BindOk>()},
              {queue::UNBIND_METHOD_ID, makeMethod<queue::Unbind>()},
              {queue::UNBINDOK_METHOD_ID, makeMethod<queue::UnbindOk>()},
              {queue::PURGE_METHOD_ID, makeMethod<queue::Purge>()},
              {queue::PURGEOK_METHOD_ID, makeMethod<queue::PurgeOk>()},
              {queue::DELETE_METHOD_ID, makeMethod<queue::Delete>()},
              {queue::DELETEOK_METHOD_ID, makeMethod<queue::DeleteOk>()}}}};
  };

  class FrameFactory {
  public:
    static void
    writeMethod(const std::shared_ptr<amqp::Method> &method,
                const std::shared_ptr<BaseConnection> &connection_type,
                const ShortUint &channel);

    template <class T>
    static auto
    readFrame(const std::shared_ptr<BaseConnection> &connection_type)
        -> std::shared_ptr<T> {
      Frame protocol_response(connection_type);
      // read incoming Frame
      protocol_response.read();

      if (protocol_response.getType() == nullptr ||
          *(protocol_response.getType()) != FrameBase::METHOD) {
        throw std::runtime_error("Read frame is not a method");
      }

      return std::dynamic_pointer_cast<T>(protocol_response.getMethod());
    }
  };
} // namespace amqp

#endif // RABBITMQ_CLIENT_AMQP_AMQP_H