// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "connection-methods.h"

namespace amqp::connection {

  void Start::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Start::encode not implemented");
  }

  void Start::decode(std::vector<Octet>::iterator &iter) {
    ConvertAmqpTypes::convertBufferToType(iter, version_major);

    ConvertAmqpTypes::convertBufferToType(iter, version_minor);

    ConvertAmqpTypes::convertBufferToType(iter, server_properties);

    ConvertAmqpTypes::convertBufferToType(iter, mechanisms);

    ConvertAmqpTypes::convertBufferToType(iter, locales);
  }

  void Start::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Start::getSize not implemented");
  }

  auto Start::getVersionMajor() -> Octet & { return version_major; }

  auto Start::getVersionMinor() -> Octet & { return version_minor; }

  auto Start::getServerProperties() -> PeerProperties & {
    return server_properties;
  }

  auto Start::getMechanisms() -> LongString & { return mechanisms; }

  auto Start::getLocales() -> LongString & { return locales; }

  void StartOk::encode(std::vector<Octet>::iterator &iter) {
    ConvertAmqpTypes::convertTypeToBuffer(iter, getClassId());

    ConvertAmqpTypes::convertTypeToBuffer(iter, getMethodId());

    ConvertAmqpTypes::convertTypeToBuffer(iter, client_properties);

    ConvertAmqpTypes::convertTypeToBuffer(iter, mechanism);

    ConvertAmqpTypes::convertTypeToBuffer(iter, response);

    ConvertAmqpTypes::convertTypeToBuffer(iter, locale);
  }

  void StartOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("StartOk::decode not implemented");
  }

  void StartOk::getSize(LongUint &size) {
    size += ConvertAmqpTypes::getBufferSize(client_properties);
    size += ConvertAmqpTypes::getBufferSize(mechanism);
    size += ConvertAmqpTypes::getBufferSize(response);
    size += ConvertAmqpTypes::getBufferSize(locale);
  }

  auto StartOk::getClientProperties() -> PeerProperties & {
    return client_properties;
  }

  auto StartOk::getMechanism() -> ShortString & { return mechanism; }

  auto StartOk::getResponse() -> LongString & { return response; }

  auto StartOk::getLocale() -> ShortString & { return locale; }

  void Secure::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Secure::encode not implemented");
  }

  void Secure::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Secure::decode not implemented");
  }

  void Secure::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Secure::getSize not implemented");
  }

  void SecureOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("SecureOk::encode not implemented");
  }

  void SecureOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("SecureOk::decode not implemented");
  }

  void SecureOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("SecureOk::getSize not implemented");
  }

  void Tune::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Tune::encode not implemented");
  }

  void Tune::decode(std::vector<Octet>::iterator &iter) {

    ConvertAmqpTypes::convertBufferToType(iter, channel_max);

    ConvertAmqpTypes::convertBufferToType(iter, frame_max);

    ConvertAmqpTypes::convertBufferToType(iter, heartbeat);
  }

  void Tune::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Tune::getSize not implemented");
  }

  auto Tune::getChannelMax() -> ShortUint & { return channel_max; }

  auto Tune::getFrameMax() -> LongUint & { return frame_max; }

  auto Tune::getHeartbeat() -> ShortUint & { return heartbeat; }

  void TuneOk::encode(std::vector<Octet>::iterator &iter) {
    ConvertAmqpTypes::convertTypeToBuffer(iter, getClassId());

    ConvertAmqpTypes::convertTypeToBuffer(iter, getMethodId());

    ConvertAmqpTypes::convertTypeToBuffer(iter, channel_max);

    ConvertAmqpTypes::convertTypeToBuffer(iter, frame_max);

    ConvertAmqpTypes::convertTypeToBuffer(iter, heartbeat);
  }

  void TuneOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("TuneOk::decode not implemented");
  }

  void TuneOk::getSize(LongUint &size) {
    size += ConvertAmqpTypes::getBufferSize(channel_max);
    size += ConvertAmqpTypes::getBufferSize(frame_max);
    size += ConvertAmqpTypes::getBufferSize(heartbeat);
  }

  auto TuneOk::getChannelMax() -> ShortUint & { return channel_max; }

  auto TuneOk::getFrameMax() -> LongUint & { return frame_max; }

  auto TuneOk::getHeartbeat() -> ShortUint & { return heartbeat; }

  void Open::encode(std::vector<Octet>::iterator &iter) {
    ConvertAmqpTypes::convertTypeToBuffer(iter, getClassId());

    ConvertAmqpTypes::convertTypeToBuffer(iter, getMethodId());

    ConvertAmqpTypes::convertTypeToBuffer(iter, path);

    ConvertAmqpTypes::convertTypeToBuffer(iter, reserved1);

    ConvertAmqpTypes::convertTypeToBuffer(iter, reserved2);
  }

  void Open::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Open::decode not implemented");
  }

  void Open::getSize(LongUint &size) {
    size += ConvertAmqpTypes::getBufferSize(path);
    size += ConvertAmqpTypes::getBufferSize(reserved1);
    size += ConvertAmqpTypes::getBufferSize(reserved2);
  }

  auto Open::getPath() -> ShortString & { return path; }

  auto Open::getReserved1() -> ShortString & { return reserved1; }

  auto Open::getReserved2() -> Octet & { return reserved2; }

  void OpenOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("OpenOk::encode not implemented");
  }

  void OpenOk::decode(std::vector<Octet>::iterator &iter) {
    ConvertAmqpTypes::convertBufferToType(iter, reserved1);
  }

  void OpenOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("OpenOk::getSize not implemented");
  }

  auto OpenOk::getReserved1() -> ShortString & { return reserved1; }

  void Close::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Close::encode not implemented");
  }

  void Close::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Close::decode not implemented");
  }

  void Close::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Close::getSize not implemented");
  }

  void CloseOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("CloseOk::encode not implemented");
  }

  void CloseOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("CloseOk::decode not implemented");
  }

  void CloseOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("CloseOk::getSize not implemented");
  }

} // namespace amqp::connection
