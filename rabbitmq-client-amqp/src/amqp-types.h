// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_AMQP_TYPES_H
#define RABBITMQ_CLIENT_AMQP_TYPES_H

#include <any>
#include <cstdint>
#include <memory>
#include <unordered_map>
#include <vector>

namespace amqp {

  constexpr int VERSION_MAJOR = 9;
  constexpr int VERSION_MINOR = 1;

  using Octet = uint8_t;
  using ShortShortUint = uint8_t;
  using ShortUint = uint16_t;
  using LongUint = uint32_t;
  using LongLongUint = uint64_t;
  using ShortShortInt = int8_t;
  using ShortInt = int16_t;
  using LongInt = int32_t;
  using LongLongInt = int64_t;

  struct DecimalValue {
    Octet decimal = 0;
    LongInt value = 0;
  };

  struct ShortString {
    ShortString() = default;
    explicit ShortString(const std::string &input) {
      data = input;
      size = input.size();
    };

    auto getSize() -> Octet & { return size; }

    auto getString() -> std::string & { return data; }

  private:
    Octet size = 0;
    std::string data;
  };

  struct LongString {
    LongString() = default;
    explicit LongString(const std::string &input) {
      data = input;
      size = input.size();
    };

    auto getSize() -> LongUint & { return size; }

    auto getString() -> std::string & { return data; }

  private:
    LongUint size = 0;
    std::string data;
  };

  struct FieldArray {
    LongInt size = 0;
    std::vector<Octet> values;
  };

  struct FieldNameValuePair {
    ShortString field_name;
    Octet value_type = 0;
    std::any field_value;
  };

  struct FieldTable {
    LongUint size = 0;
    std::unordered_map<std::string, FieldNameValuePair> field_table;
  };
  using PeerProperties = FieldTable;

  class ProtocolHeader {
  public:
    static auto getHeader() -> const std::vector<Octet> & {
      static const std::vector<Octet> header = std::vector<Octet>{
          'A', 'M', 'Q', 'P', 0, 0, VERSION_MAJOR, VERSION_MINOR};
      return header;
    };

  private:
    static std::vector<Octet> header;
  };

} // namespace amqp

#endif // RABBITMQ_CLIENT_AMQP_TYPES_H
