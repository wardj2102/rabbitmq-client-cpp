// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "rabbitmq-client-amqp/amqp.h"

namespace amqp {

  Frame::Frame(const std::shared_ptr<BaseConnection> &connection_type)
      : FrameBase(connection_type),
        generalFrame(GeneralFrame(connection_type)) {}

  void Frame::read() {
    generalFrame.read();

    type = std::make_shared<FrameBase::FrameTypeEnum>(
        (FrameBase::FrameTypeEnum)generalFrame.getHeader().getType());

    auto iter = generalFrame.getPayload()->begin();

    lookUpMethod(generalFrame.getHeader().getType(), iter, method);

    method->decode(iter);
  }

  void Frame::write() {

    switch (*type) {
    case (FrameBase::METHOD): {
      generalFrame.getHeader().getSize() = METHOD_FRAME_SIZE;
      method->getSize(generalFrame.getHeader().getSize());

      generalFrame.getHeader().getType() = *type;

      generalFrame.getPayload() = std::make_shared<std::vector<Octet>>(
          generalFrame.getHeader().getSize() + GENERAL_FRAME_SIZE);
      auto iter = generalFrame.getPayload()->begin() + HEADER_FRAME_SIZE;

      method->encode(iter);
      std::vector<Octet> look = *generalFrame.getPayload();

      generalFrame.write();
    }
    default: {
      break;
    }
    }
  }

  auto Frame::getGeneralFrame() -> GeneralFrame & { return generalFrame; }

  auto Frame::getType() -> std::shared_ptr<FrameBase::FrameTypeEnum> & {
    return type;
  }

  auto Frame::getMethod() -> std::shared_ptr<Method> & { return method; }

  void Frame::lookUpMethod(Octet &type, std::vector<Octet>::iterator &iter,
                           std::shared_ptr<Method> &method) {
    switch (type) {
    case (FrameBase::METHOD): {
      ShortUint class_id = 0;
      ShortUint method_id = 0;
      ConvertAmqpTypes::convertBufferToType(iter, class_id);

      ConvertAmqpTypes::convertBufferToType(iter, method_id);

      auto classLookup = methodLookUpTable.find(class_id);
      if (classLookup != methodLookUpTable.end()) {
        auto methodLookup = classLookup->second.find(method_id);
        if (methodLookup != classLookup->second.end()) {
          method = methodLookup->second;
        } else {
          throw std::runtime_error(
              "MethodLookUp: method id: " + std::to_string(method_id) +
              " is not found in class id: " + std::to_string(class_id));
        }
      } else {

        throw std::runtime_error(" MethodLookUp: class id: " +
                                 std::to_string(class_id) + " is not found.");
      }
      break;
    }
    case (FrameBase::CONTENT_HEADER): {
      throw std::runtime_error("content not implemented");

      break;
    }
    case (FrameBase::BODY): {

      throw std::runtime_error("body not implemented");
      break;
    }
    case (FrameBase::HEARTBEAT): {

      throw std::runtime_error("heartbeat not implemented");
      break;
    }
    default:
      break;
    }
  }

  void FrameFactory::writeMethod(
      const std::shared_ptr<amqp::Method> &method,
      const std::shared_ptr<BaseConnection> &connection_type,
      const ShortUint &channel) {
    Frame output_frame(connection_type);
    output_frame.getMethod() = method;
    output_frame.getType() =
        std::make_shared<FrameBase::FrameTypeEnum>(FrameBase::METHOD);
    output_frame.getGeneralFrame().getHeader().getChannel() = channel;
    output_frame.write();
  }

} // namespace amqp