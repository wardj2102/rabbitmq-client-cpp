// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_CONVERT_HELPERS_H
#define RABBITMQ_CLIENT_CONVERT_HELPERS_H

#include "amqp-types.h"
#include <boost/endian/conversion.hpp>
#include <boost/predef/other/endian.h>

#include <algorithm>

namespace amqp {

  class ConvertAmqpTypes {
  public:
    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    Octet &output);

    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    Octet &input);

    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    ShortShortInt &output);

    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    ShortShortInt &input);

    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    bool &output);

    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    bool &input);

    template <class T>
    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    T &output);

    template <class T>
    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    T &input);

    template <class T> static auto getBufferSize(T &input) -> LongUint {
      return sizeof(input);
    }

    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    DecimalValue &output);

    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    DecimalValue &input);

    static auto getBufferSize(DecimalValue &input) -> LongUint;

    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    ShortString &output);

    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    ShortString &input);

    static auto getBufferSize(ShortString &input) -> LongUint;

    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    LongString &output);

    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    LongString &input);

    static auto getBufferSize(LongString &input) -> LongUint;

    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    PeerProperties &output);

    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    PeerProperties &input);

    static auto getBufferSize(PeerProperties &input) -> LongUint;

    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    FieldArray &output);

    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    FieldArray &input);

    static auto getBufferSize(FieldArray &input) -> LongUint;

    static void convertBufferToType(std::vector<Octet>::iterator &iter,
                                    FieldNameValuePair &output);

    static void convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                    FieldNameValuePair &input);

    static auto getBufferSize(FieldNameValuePair &input) -> LongUint;

  private:
    template <class T>
    static inline void reverseBuffer(std::vector<Octet>::iterator &iter);
  };
} // namespace amqp
#endif // RABBITMQ_CLIENT_CONVERT_HELPERS_H
