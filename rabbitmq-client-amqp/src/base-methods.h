// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_BASE_METHODS_H
#define RABBITMQ_CLIENT_BASE_METHODS_H

#include "amqp-types.h"

namespace amqp {

  class Method {
  public:
    Method(ShortUint class_id, ShortUint method_id);
    virtual void encode(std::vector<Octet>::iterator &iter) = 0;
    virtual void decode(std::vector<Octet>::iterator &iter) = 0;
    virtual void getSize(LongUint &size) = 0;

  protected:
    auto getClassId() -> ShortUint &;
    auto getMethodId() -> ShortUint &;

  private:
    ShortUint class_id;
    ShortUint method_id;
  };

} // namespace amqp
#endif // RABBITMQ_CLIENT_BASE_METHODS_H
