// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "convert-helpers.h"

namespace amqp {

  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             Octet &output) {
    output = *iter;
    iter++;
  }

  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             Octet &input) {
    *iter = input;
    iter++;
  }

  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             ShortShortInt &output) {
    output = *iter;
    iter++;
  }

  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             ShortShortInt &input) {
    *iter = input;
    iter++;
  }

  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             bool &output) {
    output = (bool)*iter;
    iter++;
  }

  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             bool &input) {
    *iter = (Octet)input;
    iter++;
  }

  template <class T>
  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             T &output) {

#if BOOST_ENDIAN_LITTLE_BYTE
    reverseBuffer<T>(iter);
#endif

    output = *(T *)&(*iter);

    iter += sizeof(output);
  }

  template <class T>
  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             T &input) {

    *(T *)&(*iter) = input;

#if BOOST_ENDIAN_LITTLE_BYTE
    reverseBuffer<T>(iter);
#endif

    iter += sizeof(input);
  }

  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             DecimalValue &output) {
    convertBufferToType(iter, output.decimal);

    convertBufferToType(iter, output.value);
  }

  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             DecimalValue &input) {
    convertTypeToBuffer(iter, input.decimal);

    convertTypeToBuffer(iter, input.value);
  }

  auto ConvertAmqpTypes::getBufferSize(
      DecimalValue &input) // NOLINT(misc-unused-parameters)
      -> LongUint {
    throw std::runtime_error("getBufferSize DecimalValue not implemented");
  }

  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             ShortString &output) {
    convertBufferToType(iter, output.getSize());
    output.getString().resize(output.getSize());
    for (auto &i : output.getString()) {
      i = *iter;
      iter++;
    }
  }

  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             ShortString &input) {
    convertTypeToBuffer(iter, input.getSize());

    for (auto &i : input.getString()) {
      *iter = i;
      iter++;
    }
  }

  auto ConvertAmqpTypes::getBufferSize(ShortString &input) -> LongUint {
    return input.getSize() + getBufferSize(input.getSize());
  }

  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             LongString &output) {
    convertBufferToType(iter, output.getSize());
    output.getString().resize(output.getSize());
    for (auto &i : output.getString()) {
      i = *iter;
      iter++;
    }
  }

  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             LongString &input) {
    convertTypeToBuffer(iter, input.getSize());

    for (auto &i : input.getString()) {
      *iter = i;
      iter++;
    }
  }

  auto ConvertAmqpTypes::getBufferSize(LongString &input) -> LongUint {
    return input.getSize() + getBufferSize(input.getSize());
  }

  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             FieldArray &output) {
    convertBufferToType(iter, output.size);

    output.values.resize(output.size);

    for (auto &i : output.values) {
      i = *iter;
      iter++;
    }
  }

  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             FieldArray &input) {
    convertTypeToBuffer(iter, input.size);

    for (auto &i : input.values) {
      *iter = i;
      iter++;
    }
  }

  auto ConvertAmqpTypes::getBufferSize(
      FieldArray &input) // NOLINT(misc-unused-parameters)
      -> LongUint {
    throw std::runtime_error("getBufferSize FieldArray not implemented");
  }

  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             FieldNameValuePair &output) {
    convertBufferToType(iter, output.field_name);

    convertBufferToType(iter, output.value_type);

    switch ((char)output.value_type) {
    // 't' boolean
    case 't': {
      bool result = true;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'b' short-short-int
    case 'b': {
      ShortShortInt result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'B' short-short-uint
    case 'B': {
      ShortShortUint result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'U' short-int
    case 'U': {
      ShortInt result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'u' short-uint
    case 'u': {
      ShortUint result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'I' long-int
    case 'I': {
      LongInt result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'i' long-uint
    case 'i': {
      LongUint result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    //'L' long-long-int
    case 'L': {
      LongLongInt result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'l' long-long-uint
    case 'l': {
      LongLongUint result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'f' float
    case 'f': {
      float result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'd' double
    case 'd': {
      double result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'D' decimal-value
    case 'D': {
      DecimalValue result;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 's' short-string
    case 's': {
      ShortString result;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'S' long-string
    case 'S': {
      LongString result;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'A' field-array
    case 'A': {
      FieldArray result;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'T' timestamp
    case 'T': {
      LongLongUint result = 0;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
    // 'F' field - table
    case 'F': {
      FieldTable result;
      convertBufferToType(iter, result);
      output.field_value = result;
      break;
    }
      // 'V'  no field
    case 'V': {
      break;
    }
    }
  }

  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             FieldNameValuePair &input) {
    convertTypeToBuffer(iter, input.field_name);

    convertTypeToBuffer(iter, input.value_type);

    switch ((char)input.value_type) {
    // 't' boolean
    case 't': {
      bool result = std::any_cast<bool>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'b' short-short-int
    case 'b': {
      auto result = std::any_cast<ShortShortInt>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'B' short-short-uint
    case 'B': {
      auto result = std::any_cast<ShortShortUint>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'U' short-int
    case 'U': {
      auto result = std::any_cast<ShortInt>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'u' short-uint
    case 'u': {
      auto result = std::any_cast<ShortUint>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'I' long-int
    case 'I': {
      auto result = std::any_cast<LongInt>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'i' long-uint
    case 'i': {
      auto result = std::any_cast<LongUint>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    //'L' long-long-int
    case 'L': {
      auto result = std::any_cast<LongLongInt>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'l' long-long-uint
    case 'l': {
      auto result = std::any_cast<LongLongUint>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'f' float
    case 'f': {
      auto result = std::any_cast<float>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'd' double
    case 'd': {
      auto result = std::any_cast<double>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'D' decimal-value
    case 'D': {
      auto result = std::any_cast<DecimalValue>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 's' short-string
    case 's': {
      auto result = std::any_cast<ShortString>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'S' long-string
    case 'S': {
      auto result = std::any_cast<LongString>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'A' field-array
    case 'A': {
      auto result = std::any_cast<FieldArray>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'T' timestamp
    case 'T': {
      auto result = std::any_cast<LongLongUint>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
    // 'F' field - table
    case 'F': {
      auto result = std::any_cast<FieldTable>(input.field_value);
      convertTypeToBuffer(iter, result);
      break;
    }
      // 'V'  no field
    case 'V': {
      break;
    }
    }
  }

  auto ConvertAmqpTypes::getBufferSize(
      FieldNameValuePair &input) // NOLINT(misc-unused-parameters)
      -> LongUint {
    throw std::runtime_error(
        "getBufferSize FieldNameValuePair not implemented");
  }

  void ConvertAmqpTypes::convertBufferToType(std::vector<Octet>::iterator &iter,
                                             PeerProperties &output) {
    convertBufferToType(iter, output.size);
    auto begin = iter;

    while (iter < begin + output.size) {
      FieldNameValuePair pair;
      convertBufferToType(iter, pair);

      std::string key(pair.field_name.getString().begin(),
                      pair.field_name.getString().end());
      output.field_table.insert(
          std::pair<std::string, FieldNameValuePair>(key, pair));
    }
  }

  void ConvertAmqpTypes::convertTypeToBuffer(std::vector<Octet>::iterator &iter,
                                             PeerProperties &input) {
    convertTypeToBuffer(iter, input.size);

    for (auto &pair : input.field_table) {
      convertTypeToBuffer(iter, pair.second);
    }
  }

  auto ConvertAmqpTypes::getBufferSize(PeerProperties &input) -> LongUint {
    return input.size + getBufferSize(input.size);
  }

  template <class T>
  void ConvertAmqpTypes::reverseBuffer(std::vector<Octet>::iterator &iter) {
    std::reverse(iter, iter + sizeof(T));
  }
} // namespace amqp
