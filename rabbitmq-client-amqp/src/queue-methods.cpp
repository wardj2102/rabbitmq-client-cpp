// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "queue-methods.h"

namespace amqp::queue {

  void Declare::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Declare::encode not implemented");
  }

  void Declare::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Declare::decode not implemented");
  }

  void Declare::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Declare::getSize not implemented");
  }

  void DeclareOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeclareOk::encode not implemented");
  }

  void DeclareOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeclareOk::decode not implemented");
  }

  void DeclareOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeclareOk::getSize not implemented");
  }

  void Bind::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Bind::encode not implemented");
  }

  void Bind::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Bind::decode not implemented");
  }

  void Bind::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Bind::getSize not implemented");
  }

  void BindOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("BindOk::encode not implemented");
  }

  void BindOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("BindOk::decode not implemented");
  }

  void BindOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("BindOk::getSize not implemented");
  }

  void Unbind::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Unbind::encode not implemented");
  }

  void Unbind::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Unbind::decode not implemented");
  }

  void Unbind::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Unbind::getSize not implemented");
  }

  void UnbindOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("UnbindOk::encode not implemented");
  }

  void UnbindOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("UnbindOk::decode not implemented");
  }

  void UnbindOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("UnbindOk::getSize not implemented");
  }

  void Purge::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Purge::encode not implemented");
  }

  void Purge::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Purge::decode not implemented");
  }

  void Purge::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Purge::getSize not implemented");
  }

  void PurgeOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("PurgeOk::encode not implemented");
  }

  void PurgeOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("PurgeOk::decode not implemented");
  }

  void PurgeOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("PurgeOk::getSize not implemented");
  }

  void Delete::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Delete::encode not implemented");
  }

  void Delete::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Delete::decode not implemented");
  }

  void Delete::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Delete::getSize not implemented");
  }

  void DeleteOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeleteOk::encode not implemented");
  }

  void DeleteOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeleteOk::decode not implemented");
  }

  void DeleteOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeleteOk::getSize not implemented");
  }

} // namespace amqp::queue
