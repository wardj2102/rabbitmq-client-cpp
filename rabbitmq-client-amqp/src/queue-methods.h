// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_QUEUE_METHODS_H
#define RABBITMQ_CLIENT_QUEUE_METHODS_H

#include "base-methods.h"
#include "convert-helpers.h"

namespace amqp::queue {

  // Queue Class IDs
  constexpr int QUEUE_CLASS_ID = 50;
  constexpr int DECLARE_METHOD_ID = 10;
  constexpr int DECLAREOK_METHOD_ID = 11;
  constexpr int BIND_METHOD_ID = 20;
  constexpr int BINDOK_METHOD_ID = 21;
  constexpr int UNBIND_METHOD_ID = 50;
  constexpr int UNBINDOK_METHOD_ID = 51;
  constexpr int PURGE_METHOD_ID = 30;
  constexpr int PURGEOK_METHOD_ID = 31;
  constexpr int DELETE_METHOD_ID = 40;
  constexpr int DELETEOK_METHOD_ID = 41;

  class Declare : public Method {
  public:
    Declare() : Method(QUEUE_CLASS_ID, DECLARE_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> ShortInt &;
    auto getQueueName() -> ShortString &;
    auto getPassive() -> Octet &;
    auto getDurable() -> Octet &;
    auto getAutoDelete() -> Octet &;
    auto getNoWait() -> Octet &;
    auto getArguments() -> FieldTable &;

  private:
    ShortInt reserved1 = 0;
    ShortString queue_name;
    Octet passive = 0;
    Octet durable = 0;
    Octet auto_delete = 0;
    Octet no_wait = 0;
    FieldTable arguments;
  };

  class DeclareOk : public Method {
  public:
    DeclareOk() : Method(QUEUE_CLASS_ID, DECLAREOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getQueueName() -> ShortString &;
    auto getMessageCount() -> LongInt &;
    auto getConsumerCount() -> LongInt &;

  private:
    ShortString queue_name;
    LongInt message_count = 0;
    LongInt consumer_count = 0;
  };

  class Bind : public Method {
  public:
    Bind() : Method(QUEUE_CLASS_ID, BIND_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> ShortInt &;
    auto getQueueName() -> ShortString &;
    auto getExchangeName() -> ShortString &;
    auto getRoutingKey() -> ShortString &;
    auto getNoWait() -> Octet &;
    auto getArguments() -> FieldTable &;

  private:
    ShortInt reserved1 = 0;
    ShortString queue_name;
    ShortString exchange_name;
    ShortString routing_key;
    Octet no_wait = 0;
    FieldTable arguments;
  };

  class BindOk : public Method {
  public:
    BindOk() : Method(QUEUE_CLASS_ID, BINDOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;
  };

  class Unbind : public Method {
  public:
    Unbind() : Method(QUEUE_CLASS_ID, UNBIND_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> ShortInt &;
    auto getQueueName() -> ShortString &;
    auto getExchangeName() -> ShortString &;
    auto getRoutingKey() -> ShortString &;
    auto getArguments() -> FieldTable &;

  private:
    ShortInt reserved1 = 0;
    ShortString queue_name;
    ShortString exchange_name;
    ShortString routing_key;
    FieldTable arguments;
  };

  class UnbindOk : public Method {
  public:
    UnbindOk() : Method(QUEUE_CLASS_ID, UNBINDOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;
  };

  class Purge : public Method {
  public:
    Purge() : Method(QUEUE_CLASS_ID, PURGE_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> ShortInt &;
    auto getQueueName() -> ShortString &;
    auto getExchangeName() -> ShortString &;
    auto getNoWait() -> Octet &;

  private:
    ShortInt reserved1 = 0;
    ShortString queue_name;
    ShortString exchange_name;
    Octet no_wait = 0;
  };

  class PurgeOk : public Method {
  public:
    PurgeOk() : Method(QUEUE_CLASS_ID, PURGEOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getMessageCount() -> LongInt &;

  private:
    LongInt message_count = 0;
  };

  class Delete : public Method {
  public:
    Delete() : Method(QUEUE_CLASS_ID, DELETE_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> ShortInt &;
    auto getQueueName() -> ShortString &;
    auto getIfUnused() -> Octet &;
    auto getIfEmpty() -> Octet &;
    auto getNoWait() -> Octet &;

  private:
    ShortInt reserved1 = 0;
    ShortString queue_name;
    Octet if_unused = 0;
    Octet if_empty = 0;
    Octet no_wait = 0;
  };

  class DeleteOk : public Method {
  public:
    DeleteOk() : Method(QUEUE_CLASS_ID, DELETEOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getMessageCount() -> LongInt &;

  private:
    LongInt message_count = 0;
  };

} // namespace amqp::queue

#endif // RABBITMQ_CLIENT_QUEUE_METHODS_H
