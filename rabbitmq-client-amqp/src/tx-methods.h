// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_TX_METHODS_H
#define RABBITMQ_CLIENT_TX_METHODS_H

#include "base-methods.h"
#include "convert-helpers.h"

namespace amqp::tx {

  // Connection Class IDs
  constexpr int TX_CLASS_ID = 90;
  constexpr int SELECT_METHOD_ID = 10;
  constexpr int SELECTOK_METHOD_ID = 11;
  constexpr int COMMIT_METHOD_ID = 20;
  constexpr int COMMITOK_METHOD_ID = 21;
  constexpr int ROLLBACK_METHOD_ID = 30;
  constexpr int ROLLBACKOK_METHOD_ID = 31;

} // namespace amqp::tx

#endif // RABBITMQ_CLIENT_TX_METHODS_H
