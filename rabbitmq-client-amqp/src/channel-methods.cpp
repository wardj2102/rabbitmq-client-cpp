// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "channel-methods.h"

namespace amqp::channel {

  void Open::encode(std::vector<Octet>::iterator &iter) {
    ConvertAmqpTypes::convertTypeToBuffer(iter, getClassId());

    ConvertAmqpTypes::convertTypeToBuffer(iter, getMethodId());

    ConvertAmqpTypes::convertTypeToBuffer(iter, reserved1);
  }

  void Open::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Open::decode not implemented");
  }

  void Open::getSize(LongUint &size) {
    size += ConvertAmqpTypes::getBufferSize(reserved1);
  }

  void OpenOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("OpenOk::encode not implemented");
  }

  void OpenOk::decode(std::vector<Octet>::iterator &iter) {
    ConvertAmqpTypes::convertBufferToType(iter, reserved1);
  }

  void OpenOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("OpenOk::getSize not implemented");
  }

  void Flow::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Flow::encode not implemented");
  }

  void Flow::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Flow::decode not implemented");
  }

  void Flow::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Flow::getSize not implemented");
  }

  void FlowOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("FlowOk::encode not implemented");
  }

  void FlowOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("FlowOk::decode not implemented");
  }

  void FlowOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("FlowOk::getSize not implemented");
  }

  void Close::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Close::encode not implemented");
  }

  void Close::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Close::decode not implemented");
  }

  void Close::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Close::getSize not implemented");
  }

  void CloseOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("CloseOk::encode not implemented");
  }

  void CloseOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("CloseOk::decode not implemented");
  }

  void CloseOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("CloseOk::getSize not implemented");
  }
} // namespace amqp::channel
