// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "base-frames.h"

namespace amqp {

  FrameBase::FrameBase(std::shared_ptr<BaseConnection> connection_type)
      : connection_type(std::move(connection_type)) {}

  HeaderFrame::HeaderFrame(std::shared_ptr<BaseConnection> connection_type)
      : FrameBase(std::move(connection_type)) {}

  void HeaderFrame::write() {}

  void HeaderFrame::read() {

    connection_type->read(payload);

    auto iter = payload.begin();

    ConvertAmqpTypes::convertBufferToType(iter, type);

    ConvertAmqpTypes::convertBufferToType(iter, channel);

    ConvertAmqpTypes::convertBufferToType(iter, size);
  }

  auto HeaderFrame::getType() -> Octet & { return type; }

  auto HeaderFrame::getChannel() -> ShortUint & { return channel; }

  auto HeaderFrame::getSize() -> LongUint & { return size; }

  GeneralFrame::GeneralFrame(
      const std::shared_ptr<BaseConnection> &connection_type)
      : FrameBase(connection_type), header(HeaderFrame(connection_type)) {}

  void GeneralFrame::write() {
    auto iter = payload->begin();

    ConvertAmqpTypes::convertTypeToBuffer(iter, header.getType());

    ConvertAmqpTypes::convertTypeToBuffer(iter, header.getChannel());

    ConvertAmqpTypes::convertTypeToBuffer(iter, header.getSize());

    iter = payload->end() - 1;

    endCode[0] = END_CODE;
    ConvertAmqpTypes::convertTypeToBuffer(iter, endCode[0]);
    connection_type->write(*payload);
  }

  void GeneralFrame::read() {

    header.read();
    payload = std::make_shared<std::vector<Octet>>(header.getSize());

    readPayload();

    readEndCode();
  }

  auto GeneralFrame::getHeader() -> HeaderFrame & { return header; }

  auto GeneralFrame::getPayload() -> std::shared_ptr<std::vector<Octet>> & {
    return payload;
  }
  void GeneralFrame::readPayload() { connection_type->read(*payload); }

  void GeneralFrame::readEndCode() { connection_type->read(endCode); }

} // namespace amqp
