// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_CONNECTION_METHODS_H
#define RABBITMQ_CLIENT_CONNECTION_METHODS_H

#include "base-methods.h"
#include "convert-helpers.h"

namespace amqp::connection {

  // Connection Class IDs
  constexpr int CONNECTION_CLASS_ID = 10;
  constexpr int START_METHOD_ID = 10;
  constexpr int STARTOK_METHOD_ID = 11;
  constexpr int SECURE_METHOD_ID = 20;
  constexpr int SECUREOK_METHOD_ID = 21;
  constexpr int TUNE_METHOD_ID = 30;
  constexpr int TUNEOK_METHOD_ID = 31;
  constexpr int OPEN_METHOD_ID = 40;
  constexpr int OPENOK_METHOD_ID = 41;
  constexpr int CLOSE_METHOD_ID = 40;
  constexpr int CLOSEOK_METHOD_ID = 41;

  class Start : public Method {
  public:
    Start() : Method(CONNECTION_CLASS_ID, START_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getVersionMajor() -> Octet &;
    auto getVersionMinor() -> Octet &;
    auto getServerProperties() -> PeerProperties &;
    auto getMechanisms() -> LongString &;
    auto getLocales() -> LongString &;

  private:
    Octet version_major = 0;
    Octet version_minor = 0;
    PeerProperties server_properties;
    LongString mechanisms;
    LongString locales;
  };

  class StartOk : public Method {
  public:
    StartOk() : Method(CONNECTION_CLASS_ID, STARTOK_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getClientProperties() -> PeerProperties &;
    auto getMechanism() -> ShortString &;
    auto getResponse() -> LongString &;
    auto getLocale() -> ShortString &;

  private:
    PeerProperties client_properties;
    ShortString mechanism;
    LongString response;
    ShortString locale;
  };

  class Secure : public Method {
  public:
    Secure() : Method(CONNECTION_CLASS_ID, SECURE_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

  private:
    LongString challenge;
  };

  class SecureOk : public Method {
  public:
    SecureOk() : Method(CONNECTION_CLASS_ID, SECUREOK_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

  private:
    LongString response;
  };

  class Tune : public Method {
  public:
    Tune() : Method(CONNECTION_CLASS_ID, TUNE_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getChannelMax() -> ShortUint &;
    auto getFrameMax() -> LongUint &;
    auto getHeartbeat() -> ShortUint &;

  private:
    ShortUint channel_max = 0;
    LongUint frame_max = 0;
    ShortUint heartbeat = 0;
  };

  class TuneOk : public Method {
  public:
    TuneOk() : Method(CONNECTION_CLASS_ID, TUNEOK_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getChannelMax() -> ShortUint &;
    auto getFrameMax() -> LongUint &;
    auto getHeartbeat() -> ShortUint &;

  private:
    ShortUint channel_max = 0;
    LongUint frame_max = 0;
    ShortUint heartbeat = 0;
  };

  class Open : public Method {
  public:
    Open() : Method(CONNECTION_CLASS_ID, OPEN_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getPath() -> ShortString &;
    auto getReserved1() -> ShortString &;
    auto getReserved2() -> Octet &;

  private:
    ShortString path = ShortString("");
    ShortString reserved1 = ShortString("");
    Octet reserved2 = 1;
  };

  class OpenOk : public Method {
  public:
    OpenOk() : Method(CONNECTION_CLASS_ID, OPENOK_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> ShortString &;

  private:
    ShortString reserved1;
  };

  class Close : public Method {
  public:
    Close() : Method(CONNECTION_CLASS_ID, CLOSE_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReplyCode() -> ShortInt &;
    auto getReplyText() -> ShortString &;
    auto getClassId() -> ShortInt &;
    auto getMethodId() -> ShortInt &;

  private:
    ShortInt reply_code = 0;
    ShortString reply_text;
    ShortInt class_id = 0;
    ShortInt method_id = 0;
  };

  class CloseOk : public Method {
  public:
    CloseOk() : Method(CONNECTION_CLASS_ID, CLOSEOK_METHOD_ID) {}

    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;
  };
} // namespace amqp::connection

#endif // RABBITMQ_CLIENT_CONNECTION_METHODS_H
