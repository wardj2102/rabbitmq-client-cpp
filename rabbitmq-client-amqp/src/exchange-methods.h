// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_EXCHANGE_METHODS_H
#define RABBITMQ_CLIENT_EXCHANGE_METHODS_H

#include "base-methods.h"
#include "convert-helpers.h"

namespace amqp::exchange {

  // Exchange Class IDs
  constexpr int EXCHANGE_CLASS_ID = 40;
  constexpr int DECLARE_METHOD_ID = 10;
  constexpr int DECLAREOK_METHOD_ID = 11;
  constexpr int DELETE_METHOD_ID = 20;
  constexpr int DELETEOK_METHOD_ID = 21;

  class Declare : public Method {
  public:
    Declare() : Method(EXCHANGE_CLASS_ID, DECLARE_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> ShortInt &;
    auto getExchangeName() -> ShortString &;
    auto getType() -> ShortString &;
    auto getPassive() -> Octet &;
    auto getDurable() -> Octet &;
    auto getAutoDelete() -> Octet &;
    auto getInternal() -> Octet &;
    auto getNoWait() -> Octet &;
    auto getArguments() -> FieldTable &;

  private:
    ShortInt reserved1 = 0;
    ShortString exchange_name;
    ShortString type;
    Octet passive = 0;
    Octet durable = 0;
    Octet auto_delete = 0;
    Octet internal = 0;
    Octet no_wait = 0;
    FieldTable arguments;
  };

  class DeclareOk : public Method {
  public:
    DeclareOk() : Method(EXCHANGE_CLASS_ID, DECLAREOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;
  };

  class Delete : public Method {
  public:
    Delete() : Method(EXCHANGE_CLASS_ID, DELETE_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> ShortInt &;
    auto getExchangeName() -> ShortString &;
    auto getIfUnused() -> Octet &;
    auto getNoWait() -> Octet &;

  private:
    ShortInt reserved1 = 0;
    ShortString exchange_name;
    Octet if_unused = 0;
    Octet no_wait = 0;
  };

  class DeleteOk : public Method {
  public:
    DeleteOk() : Method(EXCHANGE_CLASS_ID, DELETEOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;
  };

} // namespace amqp::exchange

#endif // RABBITMQ_CLIENT_EXCHANGE_METHODS_H
