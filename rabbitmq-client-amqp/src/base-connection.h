// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_AMQP_BASE_CONNECTION_H
#define RABBITMQ_CLIENT_AMQP_BASE_CONNECTION_H

#include "amqp-types.h"

namespace amqp {

  class BaseConnection {
  public:
    BaseConnection() = default;

    virtual auto connect(const std::string &hostname, const std::string &port)
        -> bool = 0;
    virtual auto read(std::vector<Octet> &payload) -> bool = 0;
    virtual auto write(const std::vector<Octet> &payload) -> bool = 0;
  };
} // namespace amqp

#endif // RABBITMQ_CLIENT_AMQP_BASE_CONNECTION_H
