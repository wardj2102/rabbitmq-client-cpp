// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "exchange-methods.h"

namespace amqp::exchange {

  void Declare::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Declare::encode not implemented");
  }

  void Declare::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Declare::decode not implemented");
  }

  void Declare::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Declare::getSize not implemented");
  }

  void DeclareOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeclareOk::encode not implemented");
  }

  void DeclareOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeclareOk::decode not implemented");
  }

  void DeclareOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeclareOk::getSize not implemented");
  }

  void Delete::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Delete::encode not implemented");
  }

  void Delete::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Delete::decode not implemented");
  }

  void Delete::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("Delete::getSize not implemented");
  }

  void DeleteOk::encode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeleteOk::encode not implemented");
  }

  void DeleteOk::decode(
      std::vector<Octet>::iterator &iter) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeleteOk::decode not implemented");
  }

  void DeleteOk::getSize(LongUint &size) { // NOLINT(misc-unused-parameters)
    throw std::runtime_error("DeleteOk::getSize not implemented");
  }

} // namespace amqp::exchange
