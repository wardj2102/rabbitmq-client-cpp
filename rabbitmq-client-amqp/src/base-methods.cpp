// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#include "base-methods.h"

namespace amqp {

  Method::Method(ShortUint class_id, ShortUint method_id)
      : class_id(class_id), method_id(method_id) {}

  auto Method::getClassId() -> ShortUint & { return class_id; }

  auto Method::getMethodId() -> ShortUint & { return method_id; }

} // namespace amqp