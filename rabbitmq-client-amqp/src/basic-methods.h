// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_BASIC_METHODS_H
#define RABBITMQ_CLIENT_BASIC_METHODS_H

#include "base-methods.h"
#include "convert-helpers.h"

namespace amqp::basic {

  // Basic Class IDs
  constexpr int BASIC_CLASS_ID = 60;
  constexpr int QOS_METHOD_ID = 10;
  constexpr int QOSOK_METHOD_ID = 11;
  constexpr int CONSUME_METHOD_ID = 20;
  constexpr int CONSUMEOK_METHOD_ID = 21;
  constexpr int CANCEL_METHOD_ID = 30;
  constexpr int CANCELOK_METHOD_ID = 31;
  constexpr int PUBLISH_METHOD_ID = 40;
  constexpr int RETURN_METHOD_ID = 50;
  constexpr int DELIVER_METHOD_ID = 60;
  constexpr int GET_METHOD_ID = 70;
  constexpr int GETOK_METHOD_ID = 71;
  constexpr int GETEMPTY_METHOD_ID = 71;
  constexpr int ACK_METHOD_ID = 80;
  constexpr int REJECT_METHOD_ID = 90;
  constexpr int RECOVER_ASYNC_METHOD_ID = 100;
  constexpr int RECOVER_METHOD_ID = 110;
  constexpr int RECOVEROK_METHOD_ID = 111;

} // namespace amqp::basic

#endif // RABBITMQ_CLIENT_BASIC_METHODS_H
