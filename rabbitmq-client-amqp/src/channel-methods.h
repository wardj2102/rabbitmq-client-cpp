// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_CHANNEL_METHODS_H
#define RABBITMQ_CLIENT_CHANNEL_METHODS_H

#include "base-methods.h"
#include "convert-helpers.h"

namespace amqp::channel {

  // Channel Class IDs
  constexpr int CHANNEL_CLASS_ID = 20;
  constexpr int OPEN_METHOD_ID = 10;
  constexpr int OPENOK_METHOD_ID = 11;
  constexpr int FLOW_METHOD_ID = 20;
  constexpr int FLOWOK_METHOD_ID = 21;
  constexpr int CLOSE_METHOD_ID = 30;
  constexpr int CLOSEOK_METHOD_ID = 31;

  class Open : public Method {
  public:
    Open() : Method(CHANNEL_CLASS_ID, OPEN_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> ShortString &;

  private:
    ShortString reserved1;
  };

  class OpenOk : public Method {
  public:
    OpenOk() : Method(CHANNEL_CLASS_ID, OPENOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReserved1() -> LongString &;

  private:
    LongString reserved1;
  };

  class Flow : public Method {
  public:
    Flow() : Method(CHANNEL_CLASS_ID, FLOW_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getActive() -> Octet &;

  private:
    Octet active = 0;
  };

  class FlowOk : public Method {
  public:
    FlowOk() : Method(CHANNEL_CLASS_ID, FLOWOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getActive() -> Octet &;

  private:
    Octet active = 0;
  };

  class Close : public Method {
  public:
    Close() : Method(CHANNEL_CLASS_ID, CLOSE_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;

    auto getReplyCode() -> ShortInt &;
    auto getReplyText() -> ShortString &;
    auto getClassId() -> ShortInt &;
    auto getMethodId() -> ShortInt &;

  private:
    ShortInt reply_code = 0;
    ShortString reply_text;
    ShortInt class_id = 0;
    ShortInt method_id = 0;
  };

  class CloseOk : public Method {
  public:
    CloseOk() : Method(CHANNEL_CLASS_ID, CLOSEOK_METHOD_ID) {}
    void encode(std::vector<Octet>::iterator &iter) final;
    void decode(std::vector<Octet>::iterator &iter) final;
    void getSize(LongUint &size) final;
  };

} // namespace amqp::channel

#endif // RABBITMQ_CLIENT_CHANNEL_METHODS_H
