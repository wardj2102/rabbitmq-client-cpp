// Copyright 2021, Joe Ward and other contributors
// SPDX-License-Identifier: mit

#ifndef RABBITMQ_CLIENT_BASE_FRAMES_H
#define RABBITMQ_CLIENT_BASE_FRAMES_H

#include "amqp-types.h"
#include "base-connection.h"
#include "convert-helpers.h"

namespace amqp {

  inline constexpr Octet END_CODE = 206;
  inline constexpr LongUint METHOD_FRAME_SIZE = 4;
  inline constexpr int HEADER_FRAME_SIZE = 7;
  inline constexpr LongUint GENERAL_FRAME_SIZE = 8;

  class FrameBase {
  public:
    FrameBase(std::shared_ptr<BaseConnection> connection_type);

    virtual void write() = 0;

    virtual void read() = 0;

    enum FrameTypeEnum {
      METHOD = 1,
      CONTENT_HEADER = 2,
      BODY = 3,
      HEARTBEAT = 8
    };

  protected:
    bool isConnected = false;
    const std::shared_ptr<BaseConnection> connection_type;
  };

  class HeaderFrame : public FrameBase {
  public:
    HeaderFrame(std::shared_ptr<BaseConnection> connection_type);

    void write() final;

    void read() final;

    auto getType() -> Octet &;
    auto getChannel() -> ShortUint &;
    auto getSize() -> LongUint &;

  private:
    Octet type = 0;
    ShortUint channel = 0;
    LongUint size = 0;

    std::vector<Octet> payload = std::vector<Octet>(HEADER_FRAME_SIZE);
  };

  class GeneralFrame : public FrameBase {
  public:
    GeneralFrame(const std::shared_ptr<BaseConnection> &connection_type);

    void write() final;

    void read() final;

    auto getHeader() -> HeaderFrame &;
    auto getPayload() -> std::shared_ptr<std::vector<Octet>> &;

  private:
    HeaderFrame header;
    std::shared_ptr<std::vector<Octet>> payload;
    std::vector<Octet> endCode = std::vector<Octet>(1);

    void readPayload();

    void readEndCode();
  };

} // namespace amqp

#endif // RABBITMQ_CLIENT_BASE_FRAMES_H
