FROM ubuntu:20.04

# disable interactive features
ENV DEBIAN_FRONTEND=noninteractive

# install build tools
RUN apt update && apt install -y cmake git build-essential \
    libboost-system1.71-dev clang-tidy libspdlog-dev

# install gtest
RUN git clone https://github.com/google/googletest.git && \ 
    cd googletest && \ 
    mkdir build && cd build && \
    cmake .. -DBUILD_SHARED_LIBS=ON && \ 
    make && \
    make install